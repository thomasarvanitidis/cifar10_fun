from tensorflow.keras import datasets
import numpy as np


def load_cifar10():
    ''' Load, normalise and cast data.
    '''
    (train_images, train_labels), (test_images, test_labels) = datasets.cifar10.load_data()

    train_images, test_images = train_images.astype(np.float32), test_images.astype(np.float32)
    train_images, test_images = train_images / 255.0, test_images / 255.0

    return (train_images, train_labels), (test_images, test_labels)
