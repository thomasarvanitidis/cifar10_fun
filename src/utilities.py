import os
import random

import tensorflow as tf
import numpy as np

# Set seeds.
def initialise_seeds(seed):
    # define a seed for reproducability
    os.environ["PYTHONHASHSEED"] = str(seed)
    random.seed(seed)
    np.random.seed(seed)
    tf.random.set_seed(seed)
