# CIFAR 10 Fun

This repository contains the material for the following tasks:

* Investigation of the CIFAR 10 and appropriate model selection from existing applications;
* Core-set selection of 1,000 images from the CIFAR 10;
* Use of the previously selected model application for transfer learning with the representative subset.

## Usage

This material present is accompanying the Google Colabs found [here](https://drive.google.com/drive/folders/1Z7WuXNZg5g5PSFJ4zj7ikhRMAdUiqO-c?usp=sharing).

To use outside the Colab Notebooks, simply:

1. Clone and make available in your working directory, i.e. `!git clone https://thomasarvanitidis@bitbucket.org/thomasarvanitidis/cifar10_fun.git`

2. Use with:

```python
from cifar10_fun.src.data import load_cifar10
(train_images, train_labels), (test_images, test_labels) = load_cifar10()
```

## Contact

Thomas Arvanitidis
